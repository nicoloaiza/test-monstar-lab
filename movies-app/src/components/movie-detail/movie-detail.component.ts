import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IMovieDetail } from 'src/models/movie-detail-interface';

import { MoviesApiService } from 'src/services/movies-api.service';

@Component({
  selector: 'movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.scss']
})
export class MovieDetailComponent implements OnInit{
    private id: string;
    public movieDetail: IMovieDetail = null;
    public isProcessing = false;

    constructor(private _moviesApiService: MoviesApiService, private route: ActivatedRoute) { 
        
    }
    
    ngOnInit() {
        this.route.params.subscribe(params => {
            this.id =  params['id'];
            this.isProcessing = true;
            this._moviesApiService.getMovie(this.id).subscribe( movieResult => {
                this.movieDetail = movieResult;
                this.isProcessing = false;
            });
        });
    }
}

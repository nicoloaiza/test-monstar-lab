import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { catchError } from 'rxjs/operators';
import { IMovieDetail } from "src/models/movie-detail-interface";
import { ISearchApiResponse } from "src/models/search-api-response-interface";

@Injectable({
    providedIn: 'root'
})
export class MoviesApiService { 
    private baseUrl = 'http://www.omdbapi.com';
    private apiKey = 'd552a23';

    constructor(private readonly _httpClient: HttpClient) {

    }

    public getMovies(search: string): Observable<ISearchApiResponse> {
        return this._httpClient.get(`${this.baseUrl}/?apikey=${this.apiKey}&s=${search}`).pipe(
            catchError( () => {
                return of(null);
            })
        );;
    }

    public getMovie(id: string): Observable<IMovieDetail> {
        return this._httpClient.get(`${this.baseUrl}/?apikey=${this.apiKey}&i=${id}`).pipe(
            catchError( () => {
                return of(null);
            })
        );;
    }
}
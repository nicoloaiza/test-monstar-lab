import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { catchError } from 'rxjs/operators';
import { IFavouriteMovie } from "src/models/favourite-movie-interface";
import { IMovieDetail } from "src/models/movie-detail-interface";
import { ISearchApiResponse } from "src/models/search-api-response-interface";

@Injectable({
    providedIn: 'root'
})
export class FavouriteMoviesApiService { 
    private baseUrl = 'http://localhost:3000/favourites';

    constructor(private readonly _httpClient: HttpClient) {

    }

    public getFavourites(): Observable<IFavouriteMovie[]> {
        return this._httpClient.get(`${this.baseUrl}`).pipe(
            catchError( () => {
                return of(null);
            })
        );;
    }

    public getFavourite(id: string): Observable<IFavouriteMovie> {
        return this._httpClient.get(`${this.baseUrl}/${id}`).pipe(
            catchError( () => {
                return of(null);
            })
        );;
    }

    public deleteFavourite(id: string): Observable<IFavouriteMovie> {
        return this._httpClient.delete(`${this.baseUrl}/${id}`).pipe(
            catchError( () => {
                return of(null);
            })
        );;
    }

    public crateFavourite(favouriteMovie: IFavouriteMovie): Observable<IFavouriteMovie> {
        return this._httpClient.post(`${this.baseUrl}`, favouriteMovie).pipe(
            catchError( () => {
                return of(null);
            })
        );;
    }
}
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MovieDetailComponent } from 'src/components/movie-detail/movie-detail.component';
import { AppComponent } from './app.component';

const routes: Routes = [
  {
    path: 'movie/:id', component: MovieDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

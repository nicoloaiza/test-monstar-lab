import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';

import { FormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';
import { MoviesApiService } from 'src/services/movies-api.service';
import { MovieDetailComponent } from 'src/components/movie-detail/movie-detail.component';
import { FavouriteMoviesApiService } from 'src/services/favourite-movies-api.service';

@NgModule({
  declarations: [
    AppComponent,
    MovieDetailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    RouterModule
  ],
  providers: [HttpClientModule, MoviesApiService, FavouriteMoviesApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }

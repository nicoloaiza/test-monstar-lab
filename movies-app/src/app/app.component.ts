import { Component, OnInit } from '@angular/core';

import { MoviesApiService } from 'src/services/movies-api.service';
import { ISearchResponse } from 'src/models/search-response.interface';
import { FavouriteMoviesApiService } from 'src/services/favourite-movies-api.service';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'movies-app';
  public searchValue = 'tenet';
  public movies: ISearchResponse[] = null;
  public isProcessing = false;

  public search(): void {
    if (this.searchValue) {
      this.isProcessing = true;
      forkJoin([this._moviesApiService.getMovies(this.searchValue), this._favouriteMoviesApiService.getFavourites()]).subscribe( result => {
        const moviesResut = result[0] && result[0].Response ? result[0].Search : [];
        if (result[1] && result[1].length) {
          moviesResut.forEach( movie => {
            if( result[1].find( favourite => favourite.imdbID === movie.imdbID ) ) {
              movie.isFavourite = true;
            }
          });
        }
        this.movies = moviesResut;
        this.isProcessing = false;
      });
    }
  }

  public switchFavourite(movie: ISearchResponse): void {
    movie.isFavourite = !movie.isFavourite;
    if (movie.isFavourite) {
      this._favouriteMoviesApiService.crateFavourite({ imdbID: movie.imdbID, name: movie.Title }).subscribe();
    } else {
      this._favouriteMoviesApiService.deleteFavourite(movie.imdbID).subscribe();
    }
  }

  constructor(private _moviesApiService: MoviesApiService, private _favouriteMoviesApiService: FavouriteMoviesApiService) { }
  ngOnInit(): void {
    this.search();
  }

}
export interface IFavouriteMovie {
    imdbID: string;
    name: string;
}
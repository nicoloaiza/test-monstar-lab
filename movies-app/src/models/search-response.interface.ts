export interface ISearchResponse {
    Poster: string;
    Title: string;
    Type: string;
    Year: number;
    imdbID: string;
    isFavourite: boolean;
}
import { ISearchResponse } from "./search-response.interface";

export interface ISearchApiResponse {
  Response: boolean;
  Search: ISearchResponse[];
  totalResults: number;
}
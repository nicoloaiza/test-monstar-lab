

export interface IMovieRatings {
    source: string;
    value: string;
}
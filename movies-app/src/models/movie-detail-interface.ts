import { IMovieRatings } from "./movie-ratings-interface";

export interface IMovieDetail {
    Actors: string;
    Awards: string;
    BoxOffice: string;
    Country: string;
    DVD: string;
    Director: string;
    Genre: string;
    Language: string;
    Metascore: string;
    Plot: string;
    Production: string;
    Rated: string;
    Ratings: IMovieRatings[];
    Released: string;
    Response: boolean;
    Runtime: string;
    Title: string;
    Type: string;
    Website: string;
    Writer: string;
    Year: number;
}
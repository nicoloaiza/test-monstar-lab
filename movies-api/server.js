
var cors = require('cors');

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000,
  mongoose = require('mongoose'),
  Favourite = require('./api/models/favouritesMovies'),
  bodyParser = require('body-parser');
  

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/favouritesMoviesdB'); 

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


var routes = require('./api/routes/favouritesRoutes');
routes(app);


app.listen(port);

console.log('movies RESTful API server started on: ' + port);
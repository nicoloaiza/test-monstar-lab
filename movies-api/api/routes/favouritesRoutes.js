module.exports = function(app) {
    var Favourites = require('../controllers/favouritesController');
  
    // todoList Routes
    app.route('/favourites')
      .get(Favourites.list_all_favourites)
      .post(Favourites.create_a_favourite);
  
  
    app.route('/favourites/:id')
      .get(Favourites.read_a_favourite)
      .put(Favourites.update_a_favourite)
      .delete(Favourites.delete_a_favourite);
  };
module.exports = function(app) {
    var Movies = require('../controllers/moviesController');
  
    // todoList Routes
    app.route('/movies')
      .get(Movies.list_all_movies)
      .post(Movies.create_a_movie);
  
  
    app.route('/movies/:id')
      .get(Movies.read_a_movie)
      .put(Movies.update_a_movie)
      .delete(Movies.delete_a_movie);
  };

'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var FavouritesMoviesSchema = new Schema({
  name: {
    type: String
  },
  imdbID: {
    type: String
  }
});

module.exports = mongoose.model('FavouritesMoviesSchema', FavouritesMoviesSchema);
'use strict';


var mongoose = require('mongoose'),
  Favourite = mongoose.model('FavouritesMoviesSchema');

exports.list_all_favourites = function(req, res) {
    Favourite.find({}, function(err, favourite) {
    if (err)
      res.send(err);
    res.json(favourite);
  });
};

exports.create_a_favourite = function(req, res) {
  var new_favourite = new Favourite(req.body);
  new_favourite.save(function(err, favourite) {
    if (err)
      res.send(err);
    res.json(favourite);
  });
};


exports.read_a_favourite = function(req, res) {
    Favourite.findById(req.params.id, function(err, favourite) {
    if (err)
      res.send(err);
    res.json(favourite);
  });
};


exports.update_a_favourite = function(req, res) {
    Favourite.findOneAndUpdate({imdbID: req.params.id}, req.body, {new: true}, function(err, favourite) {
    if (err)
      res.send(err);
    res.json(favourite);
  });
};


exports.delete_a_favourite = function(req, res) {
    Favourite.remove({
        imdbID: req.params.id
  }, function(err, favourite) {
    if (err)
      res.send(err);
    res.json({ message: 'Task successfully deleted' });
  });
};
